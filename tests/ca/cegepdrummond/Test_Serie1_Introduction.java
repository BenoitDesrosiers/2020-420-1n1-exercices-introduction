package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test_Serie1_Introduction extends SimulConsole {

    @Test
    @Order(1)
    void test_hello_world() throws Exception {
        choixMenu("1a");
        assertSortie("Hello World!", false);
    }

    @Test
    @Order(2)
    void test_print_ex1() throws Exception {
        choixMenu("1b");
        assertSortie("Bonjour", false);
    }

    @Test
    @Order(3)
    void test_print_ex2() throws Exception {
        choixMenu("1c");
        assertSortie("Bonjour", false);

    }

    @Test
    @Order(4)
    void test_print_ex3() throws Exception {
        choixMenu("1d");
        assertSortie("Bonjour", false);

    }

}